<?php

namespace app\controllers;

use Yii;
use app\models\NiveisUsuario;
use app\models\NiveisUsuarioSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * NiveisUsuarioController implements the CRUD actions for NiveisUsuario model.
 */
class NiveisUsuarioController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all NiveisUsuario models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new NiveisUsuarioSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single NiveisUsuario model.
     * @param integer $nivel
     * @param integer $usuario
     * @param string $data
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($nivel, $usuario, $data)
    {
        return $this->render('view', [
            'model' => $this->findModel($nivel, $usuario, $data),
        ]);
    }

    /**
     * Creates a new NiveisUsuario model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new NiveisUsuario();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'nivel' => $model->nivel, 'usuario' => $model->usuario, 'data' => $model->data]);
        }

        return $this->render('create', [
            'model' => $model,
        ]);
    }

    /**
     * Updates an existing NiveisUsuario model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $nivel
     * @param integer $usuario
     * @param string $data
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($nivel, $usuario, $data)
    {
        $model = $this->findModel($nivel, $usuario, $data);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'nivel' => $model->nivel, 'usuario' => $model->usuario, 'data' => $model->data]);
        }

        return $this->render('update', [
            'model' => $model,
        ]);
    }

    /**
     * Deletes an existing NiveisUsuario model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $nivel
     * @param integer $usuario
     * @param string $data
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($nivel, $usuario, $data)
    {
        $this->findModel($nivel, $usuario, $data)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the NiveisUsuario model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $nivel
     * @param integer $usuario
     * @param string $data
     * @return NiveisUsuario the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($nivel, $usuario, $data)
    {
        if (($model = NiveisUsuario::findOne(['nivel' => $nivel, 'usuario' => $usuario, 'data' => $data])) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }
}
