<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "nivel".
 *
 * @property int $nid
 * @property string $nivel
 *
 * @property Niveisusuario[] $niveisusuarios
 */
class Nivel extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'nivel';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['nivel'], 'string', 'max' => 200],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'nid' => 'ID',
            'nivel' => 'Nível',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getNiveisusuarios()
    {
        return $this->hasMany(Niveisusuario::className(), ['nivel' => 'nid']);
    }
}
