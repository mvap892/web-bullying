<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "niveisusuario".
 *
 * @property int $nivel
 * @property int $usuario
 * @property string $data
 *
 * @property Nivel $nivel0
 * @property Usuario $usuario0
 */
class Niveisusuario extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'niveisusuario';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['nivel', 'usuario', 'data'], 'required'],
            [['nivel', 'usuario'], 'integer'],
            [['data'], 'safe'],
            [['nivel', 'usuario', 'data'], 'unique', 'targetAttribute' => ['nivel', 'usuario', 'data']],
            [['nivel'], 'exist', 'skipOnError' => true, 'targetClass' => Nivel::className(), 'targetAttribute' => ['nivel' => 'nid']],
            [['usuario'], 'exist', 'skipOnError' => true, 'targetClass' => Usuario::className(), 'targetAttribute' => ['usuario' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'nivel' => 'Nível',
            'usuario' => 'Usuário',
            'data' => 'Data',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getNivel0()
    {
        return $this->hasOne(Nivel::className(), ['nid' => 'nivel']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUsuario0()
    {
        return $this->hasOne(Usuario::className(), ['id' => 'usuario']);
    }
}
