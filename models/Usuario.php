<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "usuario".
 *
 * @property int $id
 * @property string $nome
 * @property string $tipo
 *
 * @property Consulta[] $consultas
 * @property Mensagens[] $mensagens
 * @property Mensagens[] $mensagens0
 * @property Niveisusuario[] $niveisusuarios
 */
class Usuario extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'usuario';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['nome', 'tipo'], 'string', 'max' => 200],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'nome' => 'Nome',
            'tipo' => 'Tipo',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getConsultas()
    {
        return $this->hasMany(Consulta::className(), ['usuario' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getMensagens()
    {
        return $this->hasMany(Mensagens::className(), ['usuarioenviou' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getMensagens0()
    {
        return $this->hasMany(Mensagens::className(), ['usuariorecebeu' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getNiveisusuarios()
    {
        return $this->hasMany(Niveisusuario::className(), ['usuario' => 'id']);
    }
}
