<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "mensagens".
 *
 * @property int $mmid
 * @property string $datahora
 * @property string $texto
 * @property int $usuarioenviou
 * @property int $usuariorecebeu
 *
 * @property Usuario $usuarioenviou0
 * @property Usuario $usuariorecebeu0
 */
class Mensagens extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'mensagens';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['datahora'], 'safe'],
            [['usuarioenviou', 'usuariorecebeu'], 'integer'],
            [['texto'], 'string', 'max' => 200],
            [['usuarioenviou'], 'exist', 'skipOnError' => true, 'targetClass' => Usuario::className(), 'targetAttribute' => ['usuarioenviou' => 'id']],
            [['usuariorecebeu'], 'exist', 'skipOnError' => true, 'targetClass' => Usuario::className(), 'targetAttribute' => ['usuariorecebeu' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'mmid' => 'ID',
            'datahora' => 'Data e hora',
            'texto' => 'Texto',
            'usuarioenviou' => 'Enviou',
            'usuariorecebeu' => 'Receber',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUsuarioenviou0()
    {
        return $this->hasOne(Usuario::className(), ['id' => 'usuarioenviou']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUsuariorecebeu0()
    {
        return $this->hasOne(Usuario::className(), ['id' => 'usuariorecebeu']);
    }
}
