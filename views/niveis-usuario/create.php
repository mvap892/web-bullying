<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\NiveisUsuario */

$this->title = 'Criar Níveis do Usuário';
$this->params['breadcrumbs'][] = ['label' => 'Niveis Usuarios', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="niveis-usuario-criar">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
