<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\NiveisUsuario */

$this->title = $model->nivel;
$this->params['breadcrumbs'][] = ['label' => 'Niveis Usuarios', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
\yii\web\YiiAsset::register($this);
?>
<div class="niveis-usuario-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Update', ['Atualizar', 'nivel' => $model->nivel, 'usuario' => $model->usuario, 'data' => $model->data], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Delete', ['Apagar', 'nivel' => $model->nivel, 'usuario' => $model->usuario, 'data' => $model->data], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Tem certeza de que deseja excluir este item?',
                'method' => 'Postar',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'nivel',
            'usuario',
            'data',
            [
                'attribute'=>'datanasc',
                'format' => 'date',
            ],

        ],
    ]) ?>

</div>
