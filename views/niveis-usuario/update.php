<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\NiveisUsuario */

$this->title = 'Update Niveis Usuario: ' . $model->nivel;
$this->params['breadcrumbs'][] = ['label' => 'Niveis Usuarios', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->nivel, 'url' => ['view', 'nivel' => $model->nivel, 'usuario' => $model->usuario, 'data' => $model->data]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="niveis-usuario-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
