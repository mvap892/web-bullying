<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Nivel */

$this->title = 'Criar Nível';
$this->params['breadcrumbs'][] = ['label' => 'Nivels', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="nivel-criar">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
