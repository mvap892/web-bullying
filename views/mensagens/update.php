<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Mensagens */

$this->title = 'Update Mensagens: ' . $model->mmid;
$this->params['breadcrumbs'][] = ['label' => 'Mensagens', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->mmid, 'url' => ['view', 'id' => $model->mmid]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="mensagens-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
