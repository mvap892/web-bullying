<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;
use app\models\Usuario;


/* @var $this yii\web\View */
/* @var $model app\models\Mensagens */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="mensagens-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'datahora')->textInput() ?>

    <?= $form->field($model, 'texto')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'usuarioenviou')->
       dropDownList(ArrayHelper::map(Usuario::find()
           ->orderBy('nome')
           ->all(),'id','nome'),
           ['prompt' => 'Selecione um usuário'] )
?>


    <?= $form->field($model, 'usuariorecebeu')->textInput() ?>

    <div class="form-group">
        <?= Html::submitButton('Save', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
