<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\MensagensSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="mensagens-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    <?= $form->field($model, 'mmid') ?>

    <?= $form->field($model, 'datahora') ?>

    <?= $form->field($model, 'texto') ?>

    <?= $form->field($model, 'usuarioenviou') ?>

    <?= $form->field($model, 'usuariorecebeu') ?>

    <div class="form-group">
        <?= Html::submitButton('Search', ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton('Reset', ['class' => 'btn btn-outline-secondary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
