<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\Mensagens */

$this->title = $model->mmid;
$this->params['breadcrumbs'][] = ['label' => 'Mensagens', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
\yii\web\YiiAsset::register($this);
?>
<div class="mensagens-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Update', ['Atualizar', 'id' => $model->mmid], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Delete', ['Apagar', 'id' => $model->mmid], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Tem certeza de que deseja excluir este item?',
                'method' => 'Postar',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'mmid',
            'datahora',
            'texto',
            'usuarioenviou',
            'usuariorecebeu',
            [
                'attribute'=>'datanasc',
                'format' => 'date',
            ],

        ],
    ]) ?>

</div>
