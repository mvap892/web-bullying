create database trabalhofinal;
use trabalhofinal;

create table ll_perguntas(
Pid int AUTO_INCREMENT,
texto VARCHAR(100),
PRIMARY KEY (Pid));


create table ll_respostas(
Rid int AUTO_INCREMENT,
texto VARCHAR(100),
Pid_fk int,
PRIMARY KEY (Rid),
FOREIGN KEY (Pid_fk) REFERENCES ll_perguntas(Pid));

create table ll_aluno(
Aid int AUTO_INCREMENT,
nome VARCHAR(30),
email VARCHAR(40),
tel int(20),
PRIMARY KEY (Aid));

create table ll_da(
Rid_fk int,
Aid_fk int,
data date,
PRIMARY KEY(Rid_fk, Aid_fk,data));

create table ll_desabafo(
Did int AUTO_INCREMENT,
 data date,
 texto VARCHAR(100),
 Aid_fk int,
 PRIMARY KEY(Did,data),
 FOREIGN KEY (Aid_fk) REFERENCES ll_aluno(Aid));

create table ll_consultas(
Cid int AUTO_INCREMENT,
horario int,
data date,
Aid_fk int,
PRIMARY KEY(Cid,data),
FOREIGN KEY (Aid_fk) REFERENCES ll_aluno(Aid));